#!/bin/bash

# please change full_path_dir to exact path, do not end with "/"
full_path_dir="/home/aaa"
shared_dir="$full_path_dir/shared"

# please change "admin" to your admin's name
# please change "admin_pw" to your admin's password
session=`curl -sH "Content-type:application/json" http://localhost:3000/api/v1/login -d '{ "user": "admin", "password": "admin_pw" }' | tr ',' '\n' | awk -F: '/authToken/ {gsub("\"","");print $2}; /userId/ {gsub("\"","");print $3}'`

user_id=`echo "$session" | sed -n '1p'`
token_id=`echo "$session" | sed -n '2p'`

channel=`curl -sH "X-Auth-Token: $token_id" -H "X-User-Id: $user_id" http://localhost:3000/api/v1/channels.list | sed -e "s/\(,\|\[\)/\n/g" | grep -A1 _id | grep -EA1 "^{\"_id\""` 

channel_id=`echo "$channel" | awk -F: '/_id/ {gsub("\"",""); print $2}'`
channel_name=`echo "$channel" | awk -F: '/name/ {gsub("\"",""); print $2}';echo general`

for ((i=1;i<=`echo $channel_name | wc -w`;++i));do
	current_name=`echo "$channel_name" | eval "sed -n '$i""p'"`
	mkdir -p $shared_dir/$current_name/{discussion,chat_files}

	current_id=`echo "$channel_id" | eval "sed -n '$i""p'"`
	curl -sH "X-Auth-Token: $token_id" -H "X-User-Id: $user_id" "http://localhost:3000/api/v1/channels.history?roomId=$current_id" | tr ',' '\n' | grep -Ee "^\"(msg|username|ts)\":" > $shared_dir/$current_name/chat

	url=`curl -sH "X-Auth-Token: $token_id" -H "X-User-Id: $user_id" "http://localhost:3000/api/v1/channels.files?roomId=$current_id" | tr ',' '\n' | grep -E "^\"url\":" | sed -e "s/\"//g;s/^url://"`
	filename=`echo $url | sed "s/.*\///"`
	if [ $filename != "" ] 2> /dev/null;then
		curl -sH "X-Auth-Token: $token_id" -H "X-User-Id: $user_id" $url > $shared_dir/$current_name/chat_files/$filename
	fi
done

discussion=`curl -sH "X-Auth-Token: $token_id" -H "X-User-Id: $user_id" "http://localhost:3000/api/v1/rooms.get" | tr ',' '\n' | grep -Ee "^\"(fname|topic)\":" -e '{"_id":' | grep -EB3 "^\"topic\":" | grep -E "^\"(fname|topic)\"" | sed -e "s/\"//g"`

discussion_name=`echo "$discussion" | awk -F: '/fname/ {print $2}'`
discussion_in_channel=`echo "$discussion" | awk -F: '/topic/ {print $2}'`
for ((i=1;i<=`echo $discussion_name | wc -w`;++i));do
	current_discussion_name=`echo "$discussion_name" | eval "sed -n '$i""p'"`
	current_discussion_in_channel=`echo "$discussion_in_channel" | eval "sed -n '$i""p'"`
	mkdir -p $shared_dir/$current_discussion_in_channel/discussion/$current_discussion_name/chat_files
done
