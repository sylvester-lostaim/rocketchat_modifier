# Background
- Freelance tasks are always weird, but this could be the weirdest project that I had, and I was allowed to release into public, under highly freedom MIT license.
- Requester would like to have a team chat software, and allows the sysadmin to trace or retrieve everything like chatlogs, uploaded files and etc in file-based. 
- This is highly not recommended as will cause serious performance drop and larger consumed storage space, rather than just backup the MongoDB itself. (Requester insisted on this, maybe they aren't going to build a handy DB query tool for only this?)
- All related data is queried by using Restful API and dumped into a simple FHS design (see 3, in step-by-step configuration)

# Step-by-step configuration

1) Expecting there are two containers running (rocketchat and MongoDB)
   by using the non-native "docker-compose.yml" template for rocketchat application in this archive
```
CONTAINER ID        IMAGE                           COMMAND                  CREATED             STATUS              PORTS                    NAMES
5a998014fd46        rocketchat/rocket.chat:latest   "bash -c 'for i in `…"   4 hours ago         Up 4 hours          0.0.0.0:3000->3000/tcp   work_rocketchat_1
a990a199023c        mongo:4.0                       "docker-entrypoint.s…"   4 hours ago         Up 4 hours          27017/tcp                work_mongo_1
```

2) the run.sh script should be placed where the places these two dockers were fired up.
   please change full path directory at line 4
   please change the admin username & password in run.sh at line 7

3) filesystem hierarchy standard
```
example_directroy
├── data
│   └── db
├── docker-compose.yml
├── README
├── run.sh
├── shared
│   ├── asdfafwqr324                         # channel                   
│   │   ├── chat                                   # chat_log  
│   │   ├── chat_files                             # uploaded_files
│   │   └── discussion                       # discussion
│   │       └── try1
│   │           └── chat_files
│   ├── asdfasdfasdf
│   │   ├── chat
│   │   ├── chat_files
│   │   │   └── aaa.txt
│   │   └── discussion
│   └── general
│       ├── chat
│       ├── chat_files
│       │   └── adfadsfl.txt
│       └── discussion
│           └── 234general
│               └── chat_files
└── uploads
```
4) create an hourly root user cronjob for run.sh script
   example:
        0 * * * * /home/aaa/example_directory/run.sh

5) the "shared" directory in this archive is just for reference purpose.
